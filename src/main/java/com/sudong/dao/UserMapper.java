package com.sudong.dao;

import com.sudong.pojo.User;

import java.util.Set;

public interface UserMapper {
    public User findUserById(String id);
    public Set<String> findUserRolesById(String id);
    public Set<String> findUserPermsById(String id);
}
