package com.sudong.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;


@Controller
public class LoginController {

    @RequestMapping("/userLogin")
    public String userLogin(HttpServletRequest req){

        Session session = SecurityUtils.getSubject().getSession();

        String exceptionClassName = (String) req.getAttribute("shiroLoginFailure");

        if(exceptionClassName!=null){
            if (UnknownAccountException.class.getName().equals(exceptionClassName)) {
                System.out.println("账号不存在");
            } else if (IncorrectCredentialsException.class.getName().equals(exceptionClassName)) {
                System.out.println("密码错误");
            } else if("randomCodeError".equals(exceptionClassName)){
                System.out.println("验证码错误");
            } else{
                System.out.println("未知错误");
            }
        }
        return "login";
    }

}
