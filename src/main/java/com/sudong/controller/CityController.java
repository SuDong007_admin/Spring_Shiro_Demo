package com.sudong.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CityController {

    @RequiresRoles("user_admin")
    @RequiresPermissions("item:query")
    @RequestMapping("/city")
    public String showCity(){
        return "success";
    }
}
