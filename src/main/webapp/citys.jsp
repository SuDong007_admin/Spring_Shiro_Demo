<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sudong
  Date: 2019/4/26
  Time: 9:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>CountryCode</th>
            <th>District</th>
            <th>Population</th>
        </tr>
        <c:forEach items="${citys}" var="city">
            <tr>
                <td>${city.id}</td>
                <td>${city.name}</td>
                <td>${city.countrycode}</td>
                <td>${city.district}</td>
                <td>${city.population}</td>
            </tr>
        </c:forEach>

    </table>
</body>
</html>
